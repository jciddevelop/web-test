import React, { Component } from 'react';
import './App.css';
import Table from './components/Table'
import axios from 'axios';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    }
  }

  componentDidMount() {
      axios.get('http://localhost:8080/api/v1/hits/all', {
        headers: {'Access-Control-Allow-Origin': '*'},
      })
      .then(res =>  this.setState({data: res.data}))
      .catch(e => console.error(`.catch(${e})`))
  } 

  onDelete = id => {
      axios.delete(`http://localhost:8080/api/v1/hits/${id}`)
      .then(res => {
        this.setState(state => ({
          data: state.data.filter((row, j) => row.objectID !== id),
        }))
        alert('Registro eliminado correctamente')
      })
  }


  render() {
    console.log(this.state.data)
   return ( 
    <div style={{display: 'flex', justifyContent: 'center'}}>
      <Table 
        data = {this.state.data}
        onDelete={this.onDelete.bind(this)} 
        reload={this.state.delete}
        />
    </div>
   )}
}

export default App;
