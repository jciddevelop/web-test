import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import moment from 'moment'
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import axios from 'axios';

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const styles = theme => ({
  root: {
    width: '80%',
    marginTop: theme.spacing(2),
    overflowX: 'auto',
    align: 'center',
  },
  table: {
    minWidth: 700
  },
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
},
});


const handleClick = id =>  {
  this.props.click = id;
  console.log('this is:', id)
  axios.delete(`http://localhost:8080/api/v1/hits/${id}`)
  .then(res => console.log(res.data))
  this.componentDidMount();
}

export default class TableList extends Component {
  render() {
  const { classes, data } = this.props;
  
  return (
    <Paper className={ styles.classes.root } >
      <Table className={styles.classes.table}>
        <TableHead>
          <TableRow>
           <CustomTableCell align="center">#</CustomTableCell>  
            <CustomTableCell align="center">Type</CustomTableCell>
            <CustomTableCell align="left">Author</CustomTableCell>
            <CustomTableCell align="center">Created At</CustomTableCell>
            <CustomTableCell align="center">Eliminar</CustomTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((row, id) => (
            <TableRow className={classes.row} key={id}>
              <CustomTableCell align="left">{id+1}</CustomTableCell>
              <CustomTableCell align="left">{
                  row.title !== 'null' 
                    ? row.title
                    : row.story_title !== 'null'
                        ? row.story_title 
                        : row._highlightResult
                }
              </CustomTableCell>
              <CustomTableCell align="left">{row.author}</CustomTableCell>
              <CustomTableCell align="center">{moment(row.created_at).format('DD [de] MMMM [de] YYYY')}</CustomTableCell>
              <CustomTableCell align="center">
                <IconButton onClick={(e) => handleClick(row.objectID)}  >
                    <DeleteIcon/>
                </IconButton>
              </CustomTableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
  );
}
}

// CustomizedTable.propTypes = {
//   classes: PropTypes.object.isRequired,
// };

// export default withStyles(styles)(CustomizedTable);